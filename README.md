MODEM MONITORING

The project was created as part of a Web Development first internship, with the work on connected modems data from a dashboard.
The project was a full discovery of all used technologies. As such, ameliorations are needed, and will be worked on in the future.

The csv datas were borrowed from "https://sql.sh/736-base-donnees-villes-francaises".


Technologies and features :
    - docker
    - python
    - postgreSQL
    - grafana
    - leafletJS


# Installations required :
## Docker
    $ sudo apt-get install docker-ce docker-ce-cli containerd.io
    $ sudo apt-get install docker-ce=5:19.03.9~3-0~ubuntu-focal docker-ce-cli=5:19.03.9~3-0~ubuntu-focal containerd.io

    You can ensure everything went well by printing a test :
    $ sudo docker run hello-world

## Python
    $ sudo apt-get install python3.8

    Then install the dependencies :

    $ sudo pip3 pandas
    $ sudo pip3 numpy
    $ sudo pip3 geocoder



PostgreSQL and grafana where created as images directly in Docker.
Leaflet is included in Grafana.


# Properties

This project is the full property of Lyteco society, Lyon, FRANCE.