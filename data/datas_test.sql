-- Adminer 4.8.1 PostgreSQL 13.3 (Debian 13.3-1.pgdg100+1) dump

DROP TABLE IF EXISTS "alertes";
CREATE TABLE "lyteco"."alertes" (
    "uid" integer NOT NULL,
    "description" text,
    "status" integer,
    "opendate" date NOT NULL,
    "closedate" date,
    CONSTRAINT "alertes_pkey" PRIMARY KEY ("uid")
) WITH (oids = false);

INSERT INTO "alertes" ("uid", "description", "status", "opendate", "closedate") VALUES
(1,	'Essaie d''alerte description #1',	0,	'2021-07-01',	'2021-07-07'),
(2,	'Essai d''alertes description #2',	NULL,	'2021-04-01',	'2021-07-07');

DROP TABLE IF EXISTS "datas";
CREATE TABLE "lyteco"."datas" (
    "uid" integer NOT NULL,
    "ip" inet NOT NULL,
    "cid" character varying(20) NOT NULL,
    "latence" numeric NOT NULL,
    "lastupdate" date NOT NULL,
    "reconnection" date NOT NULL,
    "reboot" date NOT NULL,
    "operator" character varying(15),
    CONSTRAINT "datas_pkey" PRIMARY KEY ("uid")
) WITH (oids = false);

INSERT INTO "datas" ("uid", "ip", "cid", "latence", "lastupdate", "reconnection", "reboot", "operator") VALUES
(1,	'120.13.98.4',	'10.1.2.3/32         ',	-75,	'2021-06-02',	'2021-06-02',	'2021-06-02',	'Orange         '),
(2,	'120.34.56.8',	'10.3.4.5/32         ',	-110,	'2021-06-02',	'2021-06-02',	'2021-06-02',	'sfr            '),
(3,	'120.56.48.7',	'10.9.8.7/32         ',	-30,	'2021-06-02',	'2021-06-02',	'2021-06-02',	'bouygues       ');

DROP TABLE IF EXISTS "localisations";
CREATE TABLE "lyteco"."localisations" (
    "uid" integer NOT NULL,
    "latitude" numeric(18,14) NOT NULL,
    "longitude" numeric(19,15) NOT NULL,
    CONSTRAINT "localisations_pkey" PRIMARY KEY ("uid")
) WITH (oids = false);

INSERT INTO "localisations" ("uid", "latitude", "longitude") VALUES
(1,	43.60442900000000,	1.443812000000000),
(2,	4.83565900000000,	45.764043000000000),
(3,	2.77662300000000,	48.871900000000000);

DROP TABLE IF EXISTS "modems";
CREATE TABLE "lyteco"."modems" (
    "uid" integer NOT NULL,
    "serial" integer NOT NULL,
    "alias" character varying(15) NOT NULL,
    "sim" character varying(20) NOT NULL,
    "init" date NOT NULL,
    CONSTRAINT "modems_pkey" PRIMARY KEY ("uid")
) WITH (oids = false);

INSERT INTO "modems" ("uid", "serial", "alias", "sim", "init") VALUES
(1,	2334653,	'rt3g-231 ',	'1234567890123  ',	'2019-03-14'),
(2,	2316720,	'rt3g-248 ',	'2345678901234  ',	'2020-10-15'),
(3,	1378290,	'rt4g-107 ',	'3456789012345  ',	'2021-01-12');

DROP TABLE IF EXISTS "users";
CREATE TABLE "lyteco"."users" (
    "uid" integer NOT NULL,
    "lastname" character varying(50) NOT NULL,
    "firstname" character varying(25) NOT NULL,
    "address" character varying(80) NOT NULL,
    "zip" integer NOT NULL,
    "city" character varying(50) NOT NULL,
    "email" character varying(45) NOT NULL,
    "phone" character varying(15) NOT NULL,
    CONSTRAINT "users_pkey" PRIMARY KEY ("uid")
) WITH (oids = false);

INSERT INTO "users" ("uid", "lastname", "firstname", "address", "zip", "city", "email", "phone") VALUES
(1,	'Lebon',	'Nathalie',	'27, rue de Bonnet',	89886,	'Bilzen',	'nathalie.lebon@free.fr',	'+33182901765'),
(2,	'Fouquet',	'André',	'584, avenue Marine Vallet',	25854,	'Jourdan',	'andreF@gmail.com',	'+33187654563'),
(3,	'Van Hoofen',	'Amélie',	'21, chemin Lemaitre',	1351,	'Tessier',	'AVH@sfr.fr',	'+33120816515');

-- 2021-07-07 14:11:12.0521+00
