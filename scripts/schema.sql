CREATE SCHEMA IF NOT EXISTS lyteco;

CREATE TABLE datas(
    uid INTEGER NOT NULL PRIMARY key,
    ip INET NOT NULL,
    cid VARCHAR(20) NOT NULL,
    latence NUMERIC NOT NULL,
    lastUpdate DATE NOT NULL,
    reconnection DATE NOT NULL,
    reboot DATE NOT NULL,
    operator VARCHAR(15)
);

CREATE TABLE modems(
   uid INTEGER NOT NULL PRIMARY KEY,
   serial INTEGER NOT NULL,
   alias VARCHAR(15) NOT NULL,
   sim VARCHAR(20) NOT NULL,
   init DATE NOT NULL
);

CREATE TABLE users(
   uid INTEGER NOT NULL PRIMARY KEY,
   lastname VARCHAR(50) NOT NULL,
   firstname VARCHAR(25) NOT NULL,
   address VARCHAR(80) NOT NULL,
   zip INTEGER NOT NULL,
   city VARCHAR(50) NOT NULL,
   email VARCHAR(45) NOT NULL,
   phone VARCHAR(15) NOT NULL
);

CREATE TABLE localisations(
   uid INTEGER NOT NULL PRIMARY KEY,
   latitude NUMERIC(18,14) NOT NULL,
   longitude NUMERIC(19,15) NOT NULL
);

CREATE TABLE alertes(
   uid INTEGER NOT NULL PRIMARY KEY,
   description TEXT,
   status INTEGER, --0 : off 1: on 2: out of order
   opendate DATE NOT NULL,
   closedate DATE NULL
);
