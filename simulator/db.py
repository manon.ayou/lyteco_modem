import sqlite3

#define connection and cursor :

connection = sqlite3.connect('client_modem.db')

cursor = connection.cursor()


#create modems table
modems = """CREATE TABLE IF NOT EXISTS
modem(modem_id INTEGER,modem_serial INTEGER,modem_alias TEXT,modem_sim TEXT,service_date REAL)"""

cursor.execute(modems)

#create data table
datas = """CREATE TABLE IF NOT EXISTS
data(data_id INTEGER,ip TEXT,cid TEXT,latence INTEGER,rssi INTEGER,lastUpdate REAL,reconnexion REAL,reboot REAL)"""

cursor.execute(datas)

#create users table
users = """CREATE TABLE IF NOT EXISTS
users(user_id INTEGER,lastname TEXT,firstname TEXT,location TEXT, zip TEXT,city TEXT,email TEXT,phone TEXT)"""

cursor.execute(users)


#add rows to the modems table :

cursor.execute("INSERT INTO modem VALUES(1,2334653,'rt3g-271','1234567890123',2457390.62152501)")
cursor.execute("INSERT INTO modem VALUES(2,2316720,'rt3g-428','2345678901234',2457390.62152501)")
cursor.execute("INSERT INTO modem VALUES(3,1378290,'rt4g-107','3456789012345',2457390.62152501)")



#add rows to the datas table :

cursor.execute("INSERT INTO data VALUES(1,'80:AA:8B',0,0,-75,2457390.62152501,2457390.62152501,2457390.62152501)")
cursor.execute("INSERT INTO data VALUES(1,'80:28:54',0,0,-91,2457390.62152501,2457390.62152501,2457390.62152501)")
cursor.execute("INSERT INTO data VALUES(1,'80:3C:8D',0,0,-110,2457390.62152501,2457390.62152501,2457390.62152501)")


#add rows to the users table :

cursor.execute("INSERT INTO users VALUES(1,'Van Hendorf','Malina','rue des trois ponts','34680','Fabrègues','maline.vh@mail.fr','07.11.22.33.44')")
cursor.execute("INSERT INTO users VALUES(2,'Bensema','Fahid','rue Jean Moulin','32130','Samatan','bensema.fahid@mail.fr','07.43.43.12.12')")
cursor.execute("INSERT INTO users VALUES(1,'Guthieres','Jacques','rue du caducée','52240','Breuvannes-en-Bassigny','JGuthieres@mail.com','07.98.76.54.32')")

cursor.execute("SELECT * FROM users")
results = cursor.fetchall()
print(results)