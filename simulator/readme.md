TO TEST THE SIMULATOR :

Since the router requests connection to the server, and the clients request connection to the router, the following order of invoking Python scripts must be followed:

Step 1 : Launch the server script
    python3 server.py

Step 2 : Launch the router script
    python3 router.py

Step 3 : Launch the client script
    python3 client1.py
    python3 client2.py
    python3 client3.py