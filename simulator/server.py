import socket

# socket.AF_INET serves to open a socket of the most common IPv4 protocol: Address Family(AF)_INET. SOCK_STREAM means it's a TCP socket.
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the Socket, just as hosting it on the machine, like an address of the socket
server.bind(("localhost", 1010))
server.listen(3)

# The server IP and MAC are abitrary and can be modified.
server_ip = "92.10.10.10"
server_mac = "00:00:0A:BB:28:FC"
router_mac = "05:10:0A:CB:24:EF"


while True:
    routerConnection, address = server.accept()
    if(routerConnection != None):
        print(routerConnection)
        break

while True:
    ethernet_header = ""
    IP_header = ""

    message = input("\nEnter the text message to send: ")
    destination_ip = input("Enter the IP of the client to send the message to:\n1. 92.10.10.15\n2. 92.10.10.20\n3. 92.10.10.25\n")

    # Corresponding to client1, client2 and client3
    if(destination_ip == "92.10.10.15" or destination_ip == "92.10.10.20" or destination_ip == "92.10.10.25"):

        source_ip = server_ip
        IP_header = IP_header + source_ip + destination_ip

        source_mac = server_mac
        destination_mac = router_mac
        ethernet_header = ethernet_header + source_mac + destination_mac

        packet = ethernet_header + IP_header + message

        # After the connection from the router has been established, send some data to the connected client. bytes() converts the message into utf-8 encoded byte stream before being sent across the network to the destination client.
        routerConnection.send(bytes(packet, "utf-8"))

    else:
        print("Wrong client IP inputted")